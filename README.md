# Docker environments to run the telekyb3 tutorials in this repository

These instructions were tested on Ubuntu Jammy (22.04). Other recent versions
should work as well.

## Prerequisites

Before starting, please ensure that Docker is installed and configured on your
system by following the instructions
[here](https://github.com/osrf/subt/wiki/Docker%20Install).

While following the instructions, please add your user account to the docker
group in order to avoid permission issues by following the instructions under
the Troubleshooting section
[here](https://github.com/osrf/subt/wiki/Docker%20Install#troubleshooting).

If your machine has an NVIDIA GPU, make sure to also install NVIDIA Docker 2
following the instructions
[here](https://github.com/osrf/subt/wiki/Docker%20Install#install-nvidia-docker).

If you don't have an NVIDIA GPU, Gazebo GUI will use software rendering via
Mesa llvmpipe (you can check that in `~/.gz/rendering/ogre2.log` after starting
the Gazebo GUI).

(As of Oct 2023, some months after the tutorial took place,
[nvidia-docker](https://github.com/NVIDIA/nvidia-docker) is deprecated and
superseded by
[nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-container-toolkit).
As the date is beyond when the tutorial took place, we will not provide new
instructions. We advise users to follow official installation instructions from
NVIDIA.)

## Get the telekyb docker material

The following instructions are known to work on the following host:
- Ubuntu 20.04 focal
  - DELL Latitude 7490 (no nvidia board) 
- Ubuntu 22.04 jammy
  - DELL Latitude 7490 (no nvidia board) 

On your host, run the following:

```
$ cd <your workspace>
$ git clone https://gitlab.inria.fr/rainbow-public/telekyb3/telekyb3_docker.git
$ cd telekyb3_docker
```

## Option 1: Build the image locally
 
This builds the image from scratch. This gives you a working environment, but
it does not contain some extra data that we included in the images on DockerHub
intended to make parts of the tutorial possible without WiFi.

If you have an NVIDIA graphics card, build using the NVIDIA Docker base image:
```
$ ./build.bash nvidia_opengl_ubuntu22
$ ./build.bash telekyb3_tutorial
```

Otherwise, build without NVIDIA Docker:
```
$ ./build.bash telekyb3_tutorial --no-nvidia
```

A few tags will be created for the same image, for convenience of running
commands.

## Option 2: Load the image from a file

Skip this step if you built
the image locally.

If the two options above fail for you, you may be given a compressed file
(e.g. on the day of the tutorial, but please try the two options above first).

You can load the file as a Docker image, e.g.
```
$ docker load < file.tar.gz
```

This should be quick, but of course depends on your machine specifications
(took 1 minute from internal SSD on Dell XPS 15 9570 from 2018).

## Run the image

To spin up a container from an image, this should work whether you pulled from
DockerHub or built locally:
```
# If you have NVIDIA GPU
$ ./run.bash telekyb3/telekyb3_tutorial:tutorial_nvidia

# Without NVIDIA GPU
$ ./run.bash telekyb3/telekyb3_tutorial:tutorial_no_nvidia --no-nvidia
```

For convenience, if you built the image locally, this is an equivalent command
that you can tab-complete:
```
$ ./run.bash telekyb3_tutorial
# Or
$ ./run.bash telekyb3_tutorial --no-nvidia
```

You can see the list of all images with
```
$ docker images
```

You can set up an alias so that you don't have to type the command every time.
For example, replace the path and image name to yours:
```
$ alias run_telekyb3="/absolute/path/to/run.bash telekyb3/telekyb3_tutorial:tutorial_nvidia"
```

## Open another terminal in the running container

To join a running container from another terminal:
```
$ ./join.bash telekyb3/telekyb3_tutorial:tutorial_nvidia
# Or
$ ./join.bash telekyb3/telekyb3_tutorial:tutorial_no_nvidia
```

For convenience, if you built the image locally, this is an equivalent command
that you can tab-complete:
```
$ ./join.bash telekyb3_tutorial
# Or
$ ./join.bash telekyb3_tutorial --no-nvidia
```

If none of the above works, you can also find the docker NAMES of the image (`docker ps`) and run the following:

```
$ docker exec --privileged -e DISPLAY=${DISPLAY} -e LINES=`tput lines` -it <NAME> bash
```
