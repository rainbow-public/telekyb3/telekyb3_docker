#!/usr/bin/env bash

#
# Copyright (C) 2023 Open Source Robotics Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

#IMG=$(basename $1)
# Use quotes if image name contains symbols like a forward slash /, but then
# cannot use `basename`.
IMG="$1"

# Truncate last "/" in case using bash-complete for directory name
if [[ $IMG == */ ]]; then
  IMG=${IMG::-1}
fi
echo "Image: " $IMG

xhost +
containerid=$(docker ps -aqf "ancestor=${IMG}")

echo "Connect to container with ID:" ${containerid}

# Detect OS
case $(uname | tr '[:upper:]' '[:lower:]') in
  linux*)
    export HOST_OS_NAME=linux
    ;;
  darwin*)
    export HOST_OS_NAME=osx
    ;;
  msys*)
    export HOST_OS_NAME=windows
    ;;
  *)
    export HOST_OS_NAME=notset
    ;;
esac

if [ "$HOST_OS_NAME" == "osx" ]
then
  LOCAL_IP=$(ifconfig | awk '/inet /&&!/127.0.0.1/{print $2;exit}')
  echo "IP: $LOCAL_IP"
  DISPLAY_OPTS=DISPLAY=${LOCAL_IP}:0
elif [ "$HOST_OS_NAME" == "linux" ]
then
  DISPLAY_OPTS=DISPLAY
fi

docker exec --privileged -e ${DISPLAY_OPTS} -it ${containerid} bash
xhost -
